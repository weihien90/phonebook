<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    /**
     * Get the phone numbers for the contact.
     */
    public function phoneNumbers()
    {
        return $this->hasMany('App\PhoneNumber');
    }

    /**
     * Get the user who creates this contact.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
