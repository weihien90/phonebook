<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\PhoneType;
use App\PhoneNumber;
use App\Contact;
use App\User;
use App\Http\Requests\StoreContactRequest;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $my_contacts = $request->user()
            ->contacts()
            ->with('phoneNumbers.type')
            ->get()
            ->sortBy(function($contact) {
                return $contact->last_name;
            })
            ->values();

        return view('contacts.index', compact('my_contacts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $phone_types = PhoneType::all();

        return view('contacts.create', compact('phone_types'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreContactRequest $request)
    {
        try {
            $contact = new Contact;
            $contact->last_name = $request->last_name;
            $contact->first_name = $request->first_name;
            $request->user()->contacts()->save($contact);

            for ($i = 0; $i < count($request->phone_number); $i++) {
                $new_phone_number = new PhoneNumber;
                $new_phone_number->phone_number = $request->phone_number[$i];
                $new_phone_number->phone_type_id = $request->phone_type[$i];
                $contact->phoneNumbers()->save($new_phone_number);
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('error', "Sorry, some error occurred while trying to store the contact.");
        } 

        return redirect()->route('contacts.index')->with('message', "New contact ({$contact->first_name}) added.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        $this->authorize('edit', $contact);

        $phone_types = PhoneType::all();
        $primary_phone = $contact->phoneNumbers->shift();

        return view('contacts.edit', compact('contact', 'phone_types', 'primary_phone'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreContactRequest $request, Contact $contact)
    {
        $this->authorize('update', $contact);

        try {
            $contact->last_name = $request->last_name;
            $contact->first_name = $request->first_name;
            $contact->save();

            $contact->phoneNumbers()->delete();
            for ($i = 0; $i < count($request->phone_number); $i++) {
                $new_phone_number = new PhoneNumber;
                $new_phone_number->phone_number = $request->phone_number[$i];
                $new_phone_number->phone_type_id = $request->phone_type[$i];
                $contact->phoneNumbers()->save($new_phone_number);
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('error', "Sorry, some error occurred while trying to update the contact.");
        } 

        return redirect()->route('contacts.edit', $contact->id)->with('message', "Contact ({$contact->first_name}) information saved.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        $this->authorize('delete', $contact);

        try {
            $contact->phoneNumbers()->delete();
            $contact->delete();
        } catch (\Exception $e) {
            return redirect()->back()->with('error', "Sorry, some error occurred while trying to delete the contact.");
        }

        return redirect()->route('contacts.index')->with('message', "Contact ({$contact->first_name}) has been deleted.");;
    }
}
