<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhoneNumber extends Model
{
    /**
     * Get the contact this phone number belongs to.
     */
    public function contact()
    {
        return $this->belongsTo('App\Contact');
    }

    /**
     * Get the type of phone number.
     */
    public function type()
    {
        return $this->belongsTo('App\PhoneType', 'phone_type_id');
    }
}
