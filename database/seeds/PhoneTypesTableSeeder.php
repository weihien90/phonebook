<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PhoneTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $phone_types = [
            ['type' => 'Home'],
            ['type' => 'Work'],
            ['type' => 'Cellular'],
            ['type' => 'Other']
        ];

        DB::table('phone_types')->insert($phone_types);
    }
}
