# Phonebook
### A simple Phonebook webapp

---
This application is built with [Laravel 5.7 (PHP framework)](https://laravel.com/docs/5.7), [Bootstrap 4](https://getbootstrap.com/) and jQuery.


**Instructions:**

* [Install the server requirements for Laravel 5.7 ](https://laravel.com/docs/5.7/installation#installation)
* [Install Composer](https://getcomposer.org/download/)
* Clone this repo
* cd into the cloned project and install packages
```bash
composer install
```
* Setup environment file
```bash
cp .env.example .env
```
* Generate application key
```bash
php artisan key:generate
```
* Setup the database. If using MySQL, create a schema with the name 'phonebook'
* Edit the database credentials in environment file (.env):
```bash
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=phonebook
DB_USERNAME=homestead
DB_PASSWORD=secret
```
* Setup database structure by running migration/seed
```bash
php artisan migrate:refresh --seed
```
* Start the server using Laravel development server
```bash
php artisan serve
```
* Access the application on localhost - http://127.0.0.1:8000/