@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Contact - {{ $contact->first_name }} {{ $contact->last_name }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('contacts.update', $contact->id) }}">
                        @method('PUT')
                        @csrf

                        <div class="form-group row">
                            <label for="last_name" class="col-sm-4 col-form-label text-md-right">Last Name</label>

                            <div class="col-md-6">
                                <input id="last_name" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name', $contact->last_name) }}" required>

                                @if ($errors->has('last_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="first_name" class="col-sm-4 col-form-label text-md-right">First Name</label>

                            <div class="col-md-6">
                                <input id="first_name" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name', $contact->first_name) }}" required>

                                @if ($errors->has('first_name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-sm-4 col-form-label text-md-right">Phone</label>

                            <div class="col-md-6">
                                <div class="input-group mb-2">
                                    <div class="input-group-prepend">
                                        <select name="phone_type[]" class="form-control{{ $errors->has('phone_type') ? ' is-invalid' : '' }}" required>
                                            @foreach ($phone_types as $type)
                                                <option value="{{ $type->id }}" {{ $primary_phone->type == $type ? "selected='selected'" : "" }}>{{ $type->type }}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                    <input class="form-control" name="phone_number[]" value="{{ $primary_phone->phone_number }}" placeholder="+6012-3456789" pattern="[\d\+-]{8,15}" required>
                                </div>
                            </div>
                        </div>

                        <div id="phone-number-container">
                            @foreach ($contact->phoneNumbers as $number)
                                <div class="form-group row">
                                    <div class="col-md-6 offset-md-4">
                                        <div class="input-group mb-2">
                                            <div class="input-group-prepend">
                                                <select name="phone_type[]" class="form-control" required>
                                                    @foreach ($phone_types as $type)
                                                        <option value="{{ $type->id }}" {{ $number->type->id == $type->id ? "selected='selected'" : "" }}>{{ $type->type }}</option>
                                                    @endforeach
                                                </select>
                                            </div>

                                            <input class="form-control" name="phone_number[]" value="{{ $number->phone_number }}" placeholder="+6012-3456789"  pattern="[\d\+-]{8,15}" required>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <div class="col-md-6 offset-md-4">
                            @if ($errors->has('phone_number.0'))
                                <span class="invalid-feedback d-block" role="alert">
                                    <strong>{{ $errors->first('phone_number.0') }}</strong>
                                </span>
                            @endif
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <button class="btn btn-sm btn-outline-info" type="button" id="add-number-btn">
                                    <i class="fa fa-plus"></i>
                                </button>
                                <button class="btn btn-sm btn-outline-info" type="button" id="remove-number-btn">
                                    <i class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
<script id="template-add-number" type="text/template">
    <div class="form-group row">
        <div class="col-md-6 offset-md-4">
            <div class="input-group mb-2">
                <div class="input-group-prepend">
                    <select name="phone_type[]" class="form-control" required>
                        @foreach ($phone_types as $type)
                            <option value="{{ $type->id }}">{{ $type->type }}</option>
                        @endforeach
                    </select>
                </div>

                <input class="form-control" name="phone_number[]" placeholder="+6012-3456789"  pattern="[\d\+-]{8,15}" required>
            </div>
        </div>
    </div>
</script>

<script>
    $( document ).ready(function() {
        $('#add-number-btn').click( function() { 
            $('#phone-number-container').append( $('#template-add-number').html() );
        });

        $('#remove-number-btn').click( function() { 
            $('#phone-number-container').children().last().remove();
        });
    });
</script>
@endsection