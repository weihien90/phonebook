@extends('layouts.app')

@section('custom_css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css" rel="stylesheet">
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 offset-md-1">
            <a class="btn btn-primary mb-4" href="{{ route('contacts.create') }}"><i class="fa fa-user-plus"></i> Add New Contact</a>

            <div class="card-columns">
                @forelse ($my_contacts as $contact)
                    <div class="card">
                        <div class="card-body">
                            <span class="card-title">{{ $contact->last_name }}, {{ $contact->first_name }}</span>
                            <form class="float-right ml-1" action="{{ route('contacts.destroy', $contact->id) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <button class="btn btn-sm btn-outline-danger delete-btn" type="submit">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </form>
                            <a class="btn btn-sm btn-outline-info float-right" href="{{ route('contacts.edit', $contact->id) }}">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <div class="phone-numbers mt-3">
                            @foreach ($contact->phoneNumbers as $phone_number)
                                <span class="card-text">
                                    <i class="fa fa-phone"></i> {{ $phone_number->phone_number }} ({{ $phone_number->type->type }})<br />
                                </span>
                            @endforeach
                            </div>
                        </div>
                    </div>
                @empty
                    <p>You have not added any contact yet.</p>
                @endforelse
            </div>
        </div>
    </div>
</div>
@endsection

@section('custom_js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.js"></script>

<script>
$( document ).ready(function() {
    $(".delete-btn").click( function (e) {
        e.preventDefault();

        var form = event.target.form;
        var contactName = $(this).parent().siblings('.card-title').text();

        swal({
            title: "Are you sure to delete " + contactName + " from your Phonebook?",
            type: "error",
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Yes",
            showCancelButton: true,
        }, function(confirmed) {
            if (confirmed) {
                form.submit();
            }
        });
    });
});
</script>
@endsection