@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Welcome to Phonebook App!</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    @auth
                        <a href="{{ route('contacts.index') }}" class="btn btn-outline-primary">
                            <i class="fa fa-address-book"> Open My Phonebook</i>
                        </a>
                    @endauth

                    @guest
                        Please login or register to continue
                    @endguest
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
